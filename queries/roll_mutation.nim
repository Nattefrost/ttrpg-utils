import std/db_sqlite
import strutils, typetraits, unicode

var db_path: string = "../ttrpgutils.db"

let db = open(db_path, "", "", "")
let raw_result = db.getValue(sql"""SELECT description 
              FROM mutations 
              WHERE id = (SELECT abs(random() % (select count (id) from mutations) + 1) 
              AS Mutation);""")

db.close()

#let format_result = raw_result[0] & " " & raw_result[1]
#echo name(type(raw_result))
echo raw_result

