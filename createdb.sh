#!/bin/bash

db_file="ttrpgutils.db"
curses_table="create table curses(id INTEGER PRIMARY KEY AUTOINCREMENT, level INTEGER NOT NULL, description TEXT NOT NULL, effect TEXT);"
curses_csv="./curses.csv"
mutations_table="create table mutations(id INTEGER PRIMARY KEY AUTOINCREMENT, description TEXT NOT NULL);"
mutations_csv="./mutations.csv"

# remove db file if present
test -f "${db_file}" && rm -f "${db_file}"

echo "${curses_table}" | sqlite3 "${db_file}"
echo "${mutations_table}" | sqlite3 "${db_file}"


# Populate curses table from curses.csv
(echo .separator ,; echo .import ${curses_csv} curses) | sqlite3 "${db_file}"
# Populate mutations table from mutations.csv
(echo .separator ,; echo .import ${mutations_csv} mutations) | sqlite3 "${db_file}"